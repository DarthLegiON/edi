import logging

import discord
from dependency_injector.wiring import inject, Provide
from discord import Client

from bot.di import UserContainer
from config import settings, stream_log_handler, sentry_init
from db import DatabaseManager
from domain.user import UserInfoUpdateHandler, UserLeftHandler, UserRepository

sentry_init()

logging.root.addHandler(stream_log_handler)

intents = discord.Intents.all()
intents.members = True

client = Client(intents=intents)


@client.event
@inject
async def on_ready(
        user_info_update_handler: UserInfoUpdateHandler = Provide[UserContainer.user_info_update_handler],
        user_left_handler: UserLeftHandler = Provide[UserContainer.user_left_handler],
        user_repository: UserRepository = Provide[UserContainer.user_repository],
        database_manager: DatabaseManager = Provide[UserContainer.database_manager],
):
    logging.info('Logged in as %s', client.user)

    try:
        for guild in client.guilds:
            existing_members_ids = set()
            for member in guild.members:
                existing_members_ids.add(member.id)
                if not member.bot:
                    user_info_update_handler.handle(member)

            all_active_users_ids = user_repository.find_active_users_ids()

            for member_id in (all_active_users_ids - existing_members_ids):
                user_left_handler.handle(member_id)
                logging.info('Member %s has been marked as left the server', member_id)

        logging.info('All users successfully refreshed')
    except Exception as ex:  # pylint: disable=broad-exception-caught
        logging.exception(ex, exc_info=True)
    finally:
        database_manager.rollback()
        await client.close()


if __name__ == "__main__":
    container = UserContainer()
    container.init_resources()  # pylint: disable=no-member
    container.wire(modules=[__name__])  # pylint: disable=no-member

client.run(settings['token'])
