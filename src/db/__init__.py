from sqlalchemy import create_engine
from sqlalchemy.orm import Session, declarative_base

import config

Base = declarative_base()

class DatabaseManager:
    session: Session = None

    def get_session(self) -> Session:
        if self.session is None:
            engine = create_engine(config.settings['database_url'])
            self.session = Session(engine)
        return self.session

    def flush(self):
        self.session.flush()

    def commit(self):
        self.session.commit()

    def rollback(self):
        self.session.rollback()

class BaseRepository:
    _session: Session

    def __init__(self, database_manager: DatabaseManager):
        self._session = database_manager.get_session()
