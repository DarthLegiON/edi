"""add news channel settings

Revision ID: dcab57923d1b
Revises: 0982dbb91ea1
Create Date: 2024-04-10 20:09:46.624946

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = 'dcab57923d1b'
down_revision = '0982dbb91ea1'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.execute("INSERT INTO settings (code, value) VALUES ('news_channel_id','')")


def downgrade() -> None:
    op.execute("DELETE FROM settings WHERE code = 'news_channel_id'")
