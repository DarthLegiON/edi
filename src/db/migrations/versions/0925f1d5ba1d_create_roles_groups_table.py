"""create roles_groups table

Revision ID: 0925f1d5ba1d
Revises: 8e049985a924
Create Date: 2022-07-28 19:30:29.407771

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0925f1d5ba1d'
down_revision = '8e049985a924'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        'user_roles_groups',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('give_role_channel_id', sa.BigInteger, nullable=False),
        sa.Column('give_role_message_id', sa.BigInteger, nullable=False),
    )
    op.create_foreign_key(
        constraint_name='fk_role_reaction_group',
        source_table='user_roles_reactions',
        local_cols=['group_id'],
        remote_cols=['id'],
        referent_table='user_roles_groups',
    )


def downgrade() -> None:
    op.drop_constraint('fk_role_reaction_group', 'user_roles_reactions')
    op.drop_table('user_roles_groups')
