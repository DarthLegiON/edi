"""add role table

Revision ID: 9bb732cbec4f
Revises: dcab57923d1b
Create Date: 2024-04-10 22:19:28.697600

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = '9bb732cbec4f'
down_revision = 'dcab57923d1b'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('roles',
    sa.Column('id', sa.BigInteger(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )

    op.execute("INSERT INTO roles (id) SELECT distinct user_roles.role_id from user_roles")
    op.create_foreign_key(None, 'user_roles', 'roles', ['role_id'], ['id'])
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'user_roles', type_='foreignkey')
    op.drop_table('roles')
    # ### end Alembic commands ###
