"""rules setting

Revision ID: b0cd3ed76761
Revises: 16ab334c949e
Create Date: 2022-11-08 05:21:39.935891

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b0cd3ed76761'
down_revision = '16ab334c949e'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.execute("INSERT INTO settings (code, value) VALUES ('rules_channel_id','{0}')".format(''))


def downgrade() -> None:
    op.execute("DELETE FROM settings WHERE code = 'rules_channel_id'")
