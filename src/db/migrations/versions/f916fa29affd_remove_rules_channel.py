"""remove_rules_channel

Revision ID: f916fa29affd
Revises: 1846042aa111
Create Date: 2024-07-07 09:25:35.189568

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = 'f916fa29affd'
down_revision = '1846042aa111'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.execute("DELETE FROM settings WHERE code = 'rules_channel_id'")


def downgrade() -> None:
    op.execute("INSERT INTO settings (code, value) VALUES ('rules_channel_id','')")
