"""fill settings

Revision ID: 47848e51690f
Revises: cd87f1d604b8
Create Date: 2022-07-28 22:12:57.298303

"""
from alembic import op
import sqlalchemy as sa
from os import getenv


# revision identifiers, used by Alembic.
revision = '47848e51690f'
down_revision = 'cd87f1d604b8'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.execute("INSERT INTO settings (code, value) VALUES ('greetings_channel_id','{0}')".format(getenv('GREETINGS_CHANNEL_ID', '')))
    op.execute("INSERT INTO settings (code, value) VALUES ('system_channel_id','{0}')".format(getenv('SYSTEM_CHANNEL_ID', '')))
    op.execute("INSERT INTO settings (code, value) VALUES ('members_left_channel_id','{0}')".format(getenv('MEMBERS_LEFT_CHANNEL_ID', '')))


def downgrade() -> None:
    op.execute("DELETE FROM settings WHERE code in ('greetings_channel_id', 'system_channel_id', 'members_left_channel_id')")
