"""create roles table

Revision ID: 8e049985a924
Revises: 
Create Date: 2022-07-28 19:13:42.097856

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8e049985a924'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        'user_roles_reactions',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('role_name', sa.String, nullable=False),
        sa.Column('reaction_name', sa.String, nullable=False),
        sa.Column('group_id', sa.Integer),
    )


def downgrade() -> None:
    op.drop_table('user_roles_reactions')
