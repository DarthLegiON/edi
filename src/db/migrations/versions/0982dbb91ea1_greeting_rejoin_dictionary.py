"""Greeting rejoin dictionary

Revision ID: 0982dbb91ea1
Revises: d61452823676
Create Date: 2024-04-10 15:00:40.275361

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = '0982dbb91ea1'
down_revision = 'd61452823676'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.execute("INSERT INTO dictionaries (id, name) VALUES (3, 'greeting_rejoin')")

def downgrade() -> None:
    op.execute("DELETE FROM dictionaries WHERE name = 'greeting_rejoin'")
