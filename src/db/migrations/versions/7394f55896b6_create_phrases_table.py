"""create phrases table

Revision ID: 7394f55896b6
Revises: 6fc3f0546264
Create Date: 2022-10-04 01:09:45.189918

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7394f55896b6'
down_revision = '6fc3f0546264'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        'phrases',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('text', sa.Text, nullable=False),
    )


def downgrade() -> None:
    op.drop_table('phrases')
