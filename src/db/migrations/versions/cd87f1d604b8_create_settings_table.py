"""create settings table

Revision ID: cd87f1d604b8
Revises: 0925f1d5ba1d
Create Date: 2022-07-28 22:11:23.391737

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'cd87f1d604b8'
down_revision = '0925f1d5ba1d'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        'settings',
        sa.Column('code', sa.String, nullable=False, primary_key=True),
        sa.Column('value', sa.Text, nullable=False),
    )


def downgrade() -> None:
    op.drop_table('settings')
