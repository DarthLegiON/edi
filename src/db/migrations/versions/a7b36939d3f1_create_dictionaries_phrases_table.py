"""create dictionaries_phrases table

Revision ID: a7b36939d3f1
Revises: 7394f55896b6
Create Date: 2022-10-04 01:09:58.290842

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a7b36939d3f1'
down_revision = '7394f55896b6'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        'dictionaries_phrases',
        sa.Column('dictionary_id', sa.Integer, primary_key=True),
        sa.Column('phrase_id', sa.Integer, primary_key=True),
    )
    op.create_foreign_key(
        constraint_name='fk_dictionary_phrase_dictionary',
        source_table='dictionaries_phrases',
        local_cols=['dictionary_id'],
        remote_cols=['id'],
        referent_table='dictionaries',
    )
    op.create_foreign_key(
        constraint_name='fk_dictionary_phrase_phrase',
        source_table='dictionaries_phrases',
        local_cols=['phrase_id'],
        remote_cols=['id'],
        referent_table='phrases',
    )


def downgrade() -> None:
    op.drop_table('dictionaries_phrases')
