"""create dictionaries table

Revision ID: 6fc3f0546264
Revises: 47848e51690f
Create Date: 2022-10-04 01:09:30.701473

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6fc3f0546264'
down_revision = '47848e51690f'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        'dictionaries',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('name', sa.String, nullable=False),
    )


def downgrade() -> None:
    op.drop_table('dictionaries')
