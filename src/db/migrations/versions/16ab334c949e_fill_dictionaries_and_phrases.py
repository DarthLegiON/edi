"""fill dictionaries and phrases

Revision ID: 16ab334c949e
Revises: a7b36939d3f1
Create Date: 2022-10-04 01:14:18.023393

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '16ab334c949e'
down_revision = 'a7b36939d3f1'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.execute("INSERT INTO phrases (id, text) VALUES (1, '{0}, добро пожаловать на борт! Ознакомься с нашим сервером и правилами: {1}')")
    op.execute("INSERT INTO phrases (id, text) VALUES (2, '{0}, запрыгивай! Не забудь узнать побольше о нашем сервере: {1}')")
    op.execute("INSERT INTO phrases (id, text) VALUES (3, '{0}, чувствуй себя как дома! А здесь ты можешь больше узнать о нас: {1}')")
    op.execute("INSERT INTO phrases (id, text) VALUES (4, 'Мы рады тебя видеть, {0}! Но для начала обязательно пройди сюда: {1}')")
    op.execute("INSERT INTO phrases (id, text) VALUES (5, '{0}, располагайся на нашем сервере! Обязательно посети этот канал: {1}')")
    op.execute("INSERT INTO phrases (id, text) VALUES (6, '{0}, наш сервер приветствует тебя! А чтобы на нем было комфортно и удобно, тебе сюда: {1}')")
    op.execute("INSERT INTO phrases (id, text) VALUES (7, '{0}, ты новый участник нашего сервера! И начать знакомство с ним мы советуем с этого места: {1}')")
    op.execute("INSERT INTO phrases (id, text) VALUES (8, 'Пользователь *{0}* свалил в закат :(')")
    op.execute("INSERT INTO phrases (id, text) VALUES (9, '*{0}* ливнул с сервака')")
    op.execute("INSERT INTO phrases (id, text) VALUES (10, 'На сервере стало меньше на одного *{0}*')")


    op.execute("INSERT INTO dictionaries (id, name) VALUES (1, 'greeting')")
    op.execute("INSERT INTO dictionaries (id, name) VALUES (2, 'member_left')")

    op.execute("INSERT INTO dictionaries_phrases (phrase_id, dictionary_id) VALUES (1, 1)")
    op.execute("INSERT INTO dictionaries_phrases (phrase_id, dictionary_id) VALUES (2, 1)")
    op.execute("INSERT INTO dictionaries_phrases (phrase_id, dictionary_id) VALUES (3, 1)")
    op.execute("INSERT INTO dictionaries_phrases (phrase_id, dictionary_id) VALUES (4, 1)")
    op.execute("INSERT INTO dictionaries_phrases (phrase_id, dictionary_id) VALUES (5, 1)")
    op.execute("INSERT INTO dictionaries_phrases (phrase_id, dictionary_id) VALUES (6, 1)")
    op.execute("INSERT INTO dictionaries_phrases (phrase_id, dictionary_id) VALUES (7, 1)")
    op.execute("INSERT INTO dictionaries_phrases (phrase_id, dictionary_id) VALUES (8, 2)")
    op.execute("INSERT INTO dictionaries_phrases (phrase_id, dictionary_id) VALUES (9, 2)")
    op.execute("INSERT INTO dictionaries_phrases (phrase_id, dictionary_id) VALUES (10, 2)")


def downgrade() -> None:
    op.execute("DELETE FROM dictionaries_phrases")
    op.execute("DELETE FROM dictionaries")
    op.execute("DELETE FROM phrases")
