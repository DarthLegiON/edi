import logging
from typing import Annotated

from fastapi import FastAPI, HTTPException, Header, Depends
from fastapi.responses import JSONResponse

from config import sentry_init, stream_log_handler, settings
from db import DatabaseManager
from domain.user import UserRepository


class NotFoundException(Exception):
    def __init__(self, message: str):
        super().__init__()
        self.message = message

app = FastAPI()

sentry_init()

logging.root.addHandler(stream_log_handler)

database_manager = DatabaseManager()
user_repository = UserRepository(database_manager)

@app.exception_handler(HTTPException)
async def http_exception_handler(exc: HTTPException):
    return JSONResponse(
        status_code=exc.status_code,
        content={"error": {"message": exc.detail}},
    )

async def verify_token(x_auth_token: Annotated[str, Header()]):
    if x_auth_token != settings['internal_api_auth_token']:
        raise HTTPException(status_code=401, detail="X-Auth-Token header invalid")

@app.get("/")
def read_root():
    return "Welcome to me2in bot API!"

@app.get("/internal/user/{discord_user_id}", dependencies=[Depends(verify_token)])
def get_user(discord_user_id: int):
    user = user_repository.find_by_id(discord_user_id)
    if user is None:
        raise HTTPException(status_code=404, detail="User not found")

    return JSONResponse(
        content={
            "data": {
                "id": user.id,
                "nickname": user.nickname,
                "isLeft": user.is_left,
            }
        }
    )
