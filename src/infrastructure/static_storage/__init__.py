from abc import ABC
from http.client import HTTPException

from config import settings
from infrastructure import AbstractInternalApiRequest


class CannotImportFileException(Exception):
    pass

class AbstractStaticStorageRequest(AbstractInternalApiRequest, ABC):
    def get_base_url(self) -> str:
        return settings['static_storage_base_url']

    def get_auth_token(self) -> str | None:
        return settings['static_storage_auth_token']

class ImportFileRequest(AbstractStaticStorageRequest, ABC):
    def get_path(self) -> str:
        return '/api/internal/file/import/public'

    def get_method(self) -> str:
        return 'POST'

class FileImporter:
    @staticmethod
    def upload_by_url(file_url: str) -> str:
        request = ImportFileRequest()
        try:
            response = request.make_request(request_body={'sourceUrl': file_url})
        except HTTPException as err:
            raise CannotImportFileException(f'Error uploading file {file_url}.') from err

        return response['data']['url']
