import http.client
import json
from abc import ABC, abstractmethod
from urllib.parse import urlparse, urlencode


class AbstractInternalApiRequest(ABC):
    @abstractmethod
    def get_path(self) -> str:
        pass

    @abstractmethod
    def get_base_url(self) -> str:
        pass

    @abstractmethod
    def get_auth_token(self) -> str|None:
        pass

    @abstractmethod
    def get_method(self) -> str:
        pass

    def make_request(self, request_query: dict|None = None, request_body: dict|list|None = None) -> dict|list|None:
        base_url_parsed = urlparse(self.get_base_url())

        if base_url_parsed.scheme == 'https':
            connection = http.client.HTTPSConnection(base_url_parsed.hostname)
        elif base_url_parsed.scheme == 'http':
            connection = http.client.HTTPConnection(base_url_parsed.hostname)
        else:
            raise ValueError(f'Unsupported URL scheme: {base_url_parsed.scheme}')

        headers = {}
        if self.get_auth_token() is not None:
            headers['X-Auth-Token'] = self.get_auth_token()

        if request_body is not None:
            headers['Content-Type'] = 'application/json'

        if request_query is not None:
            path = f'{self.get_path()}?{urlencode(request_query)}'
        else:
            path = self.get_path()

        connection.request(self.get_method(), path, json.dumps(request_body), headers)
        response = connection.getresponse()

        if response.status < 200 or response.status >= 400:
            raise http.client.HTTPException(response.status, response.reason)

        return json.loads(response.read().decode())
