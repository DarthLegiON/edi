import logging

import discord
from dependency_injector.wiring import inject, Provide
from discord import Client

from bot.di import BoostyContainer
from config import settings, stream_log_handler, sentry_init
from db import DatabaseManager
from domain.boosty import GetBoostyUsersByLevelsHandler
from helpers.boosty import BoostersReporter

sentry_init()

logging.root.addHandler(stream_log_handler)

intents = discord.Intents.all()
intents.members = True

client = Client(intents=intents)


@client.event
@inject
async def on_ready(
        get_boosty_users_by_levels_handler: GetBoostyUsersByLevelsHandler = Provide[
            BoostyContainer.get_boosty_users_by_levels_handler],
        database_manager: DatabaseManager = Provide[BoostyContainer.users.database_manager], # pylint: disable=no-member
):
    logging.info('Logged in as %s', client.user)

    try:
        users_by_levels = get_boosty_users_by_levels_handler.handle()
        for guild in client.guilds:
            await BoostersReporter.report(guild, users_by_levels)

        logging.info('Boosty users succesfully reported')
    except Exception as ex:  # pylint: disable=broad-exception-caught
        logging.exception(ex, exc_info=True)
    finally:
        database_manager.rollback()
        await client.close()


if __name__ == "__main__":
    container = BoostyContainer()
    container.init_resources()  # pylint: disable=no-member
    container.wire(modules=[__name__])  # pylint: disable=no-member

client.run(settings['token'])
