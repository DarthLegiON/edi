def mention_user(user_id: int) -> str:
    return f'<@{user_id}>'


def mention_channel(channel_id: int) -> str:
    return f'<#{channel_id}>'
