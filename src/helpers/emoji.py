from typing import Optional

import discord


class EmojiHelper:
    @staticmethod
    def find_emoji_by_name(emoji_name: str, guild: discord.Guild) -> Optional[discord.Emoji]:
        for emoji in guild.emojis:
            if emoji.name == emoji_name:
                return emoji
        return None
