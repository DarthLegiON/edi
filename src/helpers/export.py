import logging
import re
from collections.abc import AsyncIterator
from typing import Any

from discord import Message

from infrastructure.static_storage import FileImporter, CannotImportFileException


class HistoryExporter:
    async def export_history(self, history: AsyncIterator[Message]) -> list[dict[str, Any]]:
        messages = []
        async for message in history:
            lines = message.content.splitlines()
            if len(lines) == 0:
                logging.info('Empty message skipped: %s', message.id)
                continue

            title = None
            header = None
            content = []
            tags = []

            for line in lines:
                if title is None:
                    title = self._parse_title(line)
                    continue

                if len(line) == 0:
                    if len(content) > 0:
                        content.append("")
                    continue

                if line.startswith('<@&'):
                    tags = re.findall(r'<@&([0-9]+)>', line)
                    continue

                if header is None:
                    header = line
                    continue

                if line.startswith('http'):
                    content.append(f'![Ссылка]({line})')
                    continue

                content.append(line)

            if title is None or header is None:
                continue

            picture_url = None
            images = []

            for attachment in message.attachments:
                uploaded_url = self._upload_image_to_static_storage(attachment.url)

                if picture_url is None:
                    picture_url = uploaded_url
                    continue

                images.append(f'![image]({uploaded_url})')

            if len(images) > 0:
                content.append(''.join(images))

            author_name = message.author.display_name if message.author.display_name else message.author.name

            messages.append({
                'author': author_name,
                'title': title,
                'header': header,
                'created_at': int(message.created_at.timestamp()),
                'content': "\n".join(content) if len(content) > 0 else None,
                'tags': tags if tags else None,
                'picture_url': picture_url,
            })

            logging.info(
                'Message parsed: %s, author: %s, date: %s',
                message.id,
                author_name,
                message.created_at.isoformat()
            )

        return messages

    @staticmethod
    def _parse_title(first_line: str) -> str:
        if first_line.startswith("**"):
            title = first_line.strip("**")
        else:
            title = first_line

        if len(title) > 255:
            title = 'Без заголовка'

        return title

    @staticmethod
    def _upload_image_to_static_storage(url: str) -> str:
        uploader = FileImporter()

        try:
            imported_url = uploader.upload_by_url(url)
            logging.info('Image %s imported as %s', url, imported_url)

            return imported_url
        except CannotImportFileException as e:
            logging.exception(e)

            return url
