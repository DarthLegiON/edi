import logging
from typing import Union

import discord


class ReactionsHelper:
    @staticmethod
    async def remove_reaction_from_channel_message(client: discord.Client,
                                                   emoji: Union[discord.Emoji, discord.PartialEmoji],
                                                   member: discord.Member,
                                                   channel_id: int,
                                                   message_id: int):
        channel = client.get_channel(channel_id)
        if isinstance(channel, discord.TextChannel):
            message = await channel.fetch_message(message_id)
            if message is not None:
                await message.remove_reaction(emoji, member)
                logging.info('Reaction %s by user %s has been removed from role giving message', emoji.name,
                             member.display_name)
            else:
                logging.warning('Role giving message not found')
        else:
            logging.warning('Role giving channel not found')
