from dependency_injector.wiring import Provide, inject
from discord import Guild

from bot.db import SettingRepository
from bot.di import BoostyContainer
from bot.setting import Setting
from domain.boosty import BoostyLevelWithUsers
from helpers.common import mention_user


class ChannelNotFound(Exception):
    pass

class BoostersReporter:
    @staticmethod
    @inject
    async def report(guild: Guild, users_with_boosty_levels: list[BoostyLevelWithUsers],
                     setting_repository: SettingRepository = Provide[
                         BoostyContainer.users.setting_repository]) -> None: #pylint: disable=no-member
        if not users_with_boosty_levels:
            return

        channel_id = int(setting_repository.get_value_or_fail_by_code(Setting.NEWS_CHANNEL_ID))
        channel = guild.get_channel(channel_id)
        if channel is None:
            raise ChannelNotFound(f'Channel {channel_id} not found')

        prefix = 'Напомним, что у нас есть аккаунт на Boosty:\nhttps://boosty.to/me2in\n\n\
Это помогает нам оплачивать хостинг старого сайта, разработку нового, сервер с этим ботом и бусты для сервера. \
Спасибо нашим бустерам:\n'
        fragments = []
        for users_with_boosty_level in users_with_boosty_levels:
            users_list = " ".join(map(lambda user: mention_user(user.id), users_with_boosty_level.users))
            fragments.append(f'Уровень **{users_with_boosty_level.level}**: {users_list}')

        postfix = '\n\n*Чтобы получить роль на сервере, обязательно включите интеграцию с Discord в настройках Boosty!*'

        await channel.send(prefix + '\n'.join(fragments) + postfix)
