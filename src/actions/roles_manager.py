import logging

import discord

from actions.exceptions import RoleNotFoundException


class RolesManager:
    async def assign_role_by_name(self, member: discord.member, role_name: str):
        role = self._find_role_on_server_by_name(member.guild, role_name)
        if not self._check_if_member_has_the_role(member, role):
            await member.add_roles(role, reason="Участник выбрал реакцию")
            logging.info('Role "%s" has been assigned to user %s', role_name, member.display_name)
        else:
            logging.debug('User "%s" already has role %s', member.display_name, role_name)

    async def revoke_role_by_name(self, member: discord.member, role_name: str):
        role = self._find_role_on_server_by_name(member.guild, role_name)
        if self._check_if_member_has_the_role(member, role):
            await member.remove_roles(role)
            logging.info('Role "%s" has been revoked from user %s', role_name, member.display_name)
        else:
            logging.debug('User %s has no role "%s"', member.display_name, role_name)

    @staticmethod
    def _find_role_on_server_by_name(guild: discord.guild, role_name: str) -> discord.Role:
        for role in guild.roles:
            if role.name == role_name:
                return role
        raise RoleNotFoundException(role_name)

    @staticmethod
    def _check_if_member_has_the_role(member: discord.member, role: discord.Role) -> bool:
        for member_role in member.roles:
            if member_role == role:
                return True
        return False
