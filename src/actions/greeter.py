import discord

import helpers.common
from bot.db import SettingRepository
from bot.setting import Setting
from dictionaries import Dictionary, DictionaryService


class Greeter:
    def __init__(self, dictionary_service: DictionaryService, setting_repository: SettingRepository) -> None:
        self.dictionary_service = dictionary_service
        self.setting_repository = setting_repository

    async def greet(self, member: discord.member, channel: discord.TextChannel, is_new: bool = True):
        greeting_phrase = self.dictionary_service.get_random_phrase(
            Dictionary.GREETING if is_new else Dictionary.GREETING_REJOIN
        )
        greetings_channel_id = self.setting_repository.get_value_or_fail_by_code(Setting.GREETINGS_CHANNEL_ID)
        await channel.send(greeting_phrase.format(
            helpers.common.mention_user(member.id),
            helpers.common.mention_channel(int(greetings_channel_id)),
        ))
