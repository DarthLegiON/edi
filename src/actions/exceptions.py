class RoleNotFoundException(BaseException):
    role_name: str

    def __init__(self, role_name: str, *args: object) -> None:
        super().__init__(*args)
        self.role_name = role_name
