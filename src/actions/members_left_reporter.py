import discord

from actions.channel_reporter import ChannelReporter
from dictionaries import DictionaryService, Dictionary
from domain.user import UserRepository


class MembersLeftReporter:
    def __init__(self, channel_reporter: ChannelReporter, dictionary_service: DictionaryService,
                 user_repository: UserRepository) -> None:
        self.channel_reporter = channel_reporter
        self.dictionary_service = dictionary_service
        self.user_repository = user_repository

    async def report(self, member: discord.Member, channel: discord.TextChannel):
        phrase = self.dictionary_service.get_random_phrase(Dictionary.MEMBER_LEFT)
        user = self.user_repository.find_by_id(member.id)
        if user is not None:
            nickname = user.nickname
        else:
            if member.discriminator != '0':
                nickname = f'{member.name}#{member.discriminator}'
            else:
                nickname = member.name

        await self.channel_reporter.report(
            channel,
            phrase.format(nickname)
        )
