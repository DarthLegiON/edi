from domain.boosty.db import RoleBoostyLevelRepository
from domain.user import User

class BoostyLevelWithUsers:
    def __init__(self, level: str, users: list[User]) -> None:
        self.level = level
        self.users = users

class GetBoostyUsersByLevelsHandler:
    def __init__(self, role_boosty_level_repository: RoleBoostyLevelRepository):
        self.role_boosty_level_repository = role_boosty_level_repository

    # Собирает всех бустеров по уровням
    def handle(self) -> list[BoostyLevelWithUsers]:
        users_by_levels = []
        boosty_levels = self.role_boosty_level_repository.get_all_ordered()
        if boosty_levels:
            for boosty_level in boosty_levels:
                users: list[User] = boosty_level.role.users
                if users:
                    level_with_users = BoostyLevelWithUsers(boosty_level.name, users)
                    users_by_levels.append(level_with_users)

        return users_by_levels
