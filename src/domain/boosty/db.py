import string
from typing import Type

from sqlalchemy import String, SmallInteger, ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from db import Base, BaseRepository
from domain.user.db import Role


class RoleBoostyLevel(Base):
    __tablename__ = 'roles_boosty_levels'

    role_id: Mapped[int] = mapped_column(ForeignKey('roles.id'), primary_key=True)
    role: Mapped[Role] = relationship(Role)
    name: Mapped[string] = mapped_column(String, nullable=False)
    order: Mapped[int] = mapped_column(SmallInteger, nullable=False, default=0, index=True)

class RoleBoostyLevelRepository(BaseRepository):
    def get_all_ordered(self) -> list[Type[RoleBoostyLevel]]:
        return self._session.query(RoleBoostyLevel).order_by(RoleBoostyLevel.order.desc()).all()
