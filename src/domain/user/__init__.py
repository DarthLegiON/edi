from discord import Member, Role

from db import DatabaseManager
from domain.user.db import UserRepository, User, UserRoleRepository


def get_member_nickname(member: Member) -> str:
    return member.nick if member.nick is not None else \
        (member.global_name if member.global_name is not None else member.name)

# Обновляет информацию о юзере, когда она меняется на сервере
class UserInfoUpdateHandler:
    def __init__(self, user_repository: UserRepository, database_manager: DatabaseManager,
                 user_role_repository: UserRoleRepository):
        self.user_role_repository = user_role_repository
        self.user_repository = user_repository
        self.database_manager = database_manager

    def handle(self, member: Member) -> None:
        user = self.user_repository.find_by_id_or_create(member.id)

        user.nickname = get_member_nickname(member)
        user.is_left = False

        self.user_repository.save(user)

        roles_ids = set(map(lambda x: x.id, member.roles))
        self.user_role_repository.update_roles_on_user(user, roles_ids)

        self.database_manager.commit()


class JoinUserHandlerReturnObject:
    is_new: bool = False

# Когда на сервере появляется новый юзер, добавляет его в БД или восстанавливает существующего
class JoinUserHandler:

    def __init__(self, user_repository: UserRepository, database_manager: DatabaseManager):
        self.user_repository = user_repository
        self.database_manager = database_manager

    async def handle(self, member: Member) -> JoinUserHandlerReturnObject:
        return_dto = JoinUserHandlerReturnObject()
        user = self.user_repository.find_by_id(member.id)
        if user is None:
            user = User(id=member.id)
            return_dto.is_new = True
        roles = user.roles

        if user.nickname is not None and user.nickname != member.global_name:
            await member.edit(nick=str(user.nickname), reason="Имя восстановлено после возвращения участника")
        else:
            user.nickname = get_member_nickname(member)
        user.is_left = False

        self.user_repository.save(user)

        existing_roles_ids = set(map(lambda x: x.id, roles))
        if existing_roles_ids:
            roles_to_add: list[Role] = []
            for role_id in existing_roles_ids:
                role = member.guild.get_role(role_id)
                if role is not None:
                    roles_to_add.append(member.guild.get_role(role_id))

            # тут первая роль обрезается, потому что это всегда everyone, а на него почему-то ругается дискорд.
            await member.add_roles(*roles_to_add[1:], reason="Роли восстановлены после возвращения участника")
        self.database_manager.commit()

        return return_dto

# Когда юзер выходит, делает отметку о его деактивации в БД
class UserLeftHandler:
    def __init__(self, user_repository: UserRepository, database_manager: DatabaseManager):
        self.user_repository = user_repository
        self.database_manager = database_manager

    def handle(self, user_id: int) -> None:
        user = self.user_repository.find_by_id_or_create(user_id)

        user.is_left = True

        self.user_repository.save(user)

        self.database_manager.commit()
