from typing import Optional

from sqlalchemy import ForeignKey, BigInteger, and_, Boolean, String, false
from sqlalchemy.orm import Mapped, mapped_column, relationship
from sqlalchemy.sql import expression

from db import Base, DatabaseManager, BaseRepository


class User(Base):
    __tablename__ = 'users'

    id: Mapped[int] = mapped_column(BigInteger, primary_key=True)
    nickname: Mapped[str] = mapped_column(String, nullable=False, server_default='')
    is_left: Mapped[bool] = mapped_column(Boolean, nullable=False, server_default=expression.false())
    roles = relationship('Role', secondary='user_roles', back_populates="users")

class Role(Base):
    __tablename__ = 'roles'

    id: Mapped[int] = mapped_column(BigInteger, primary_key=True)
    users = relationship(User, secondary='user_roles')

class UserRole(Base):
    __tablename__ = 'user_roles'

    user_id: Mapped[int] = mapped_column(ForeignKey('users.id'), primary_key=True)
    role_id: Mapped[int] = mapped_column(ForeignKey('roles.id', ondelete='CASCADE'), primary_key=True)

class UserRepository(BaseRepository):
    def find_by_id(self, user_id: int) -> Optional[User]:
        return self._session.query(User).filter(User.id == user_id).first()

    def find_by_id_or_create(self, user_id: int) -> User:
        user = self._session.query(User).filter(User.id == user_id).first()
        if user is None:
            user = User(id=user_id)
            self.save(user)

        return user

    def find_active_users_ids(self) -> set:
        users = self._session.query(User.id).filter(User.is_left == false()).all()
        return set(map(lambda u: u.id, users))

    def save(self, entity: User) -> None:
        self._session.add(entity)

class RoleRepository(BaseRepository):
    def find_by_id(self, role_id: int) -> Optional[Role]:
        return self._session.query(Role).filter(Role.id == role_id).first()

    def find_by_id_or_create(self, role_id: int) -> Role:
        entity = self._session.query(Role).filter(Role.id == role_id).first()
        if entity is None:
            entity = Role(id=role_id)
            self.save(entity)

        return entity

    def save(self, entity: Role) -> None:
        self._session.add(entity)

class UserRoleRepository(BaseRepository):
    def __init__(self, database_manager: DatabaseManager, role_repository: RoleRepository):
        self._role_repository = role_repository
        super().__init__(database_manager)

    def update_roles_on_user(self, user: User, roles_ids: set[int]):
        roles_to_delete = self._session.query(UserRole).filter(
            and_(UserRole.user_id == user.id, ~UserRole.role_id.in_(roles_ids))).all()
        for role in roles_to_delete:
            self._session.delete(role)

        roles_to_preserve = self._session.query(UserRole.role_id).filter(
            and_(UserRole.user_id == user.id, UserRole.role_id.in_(roles_ids))).all()
        roles_ids_to_preserve = set(map(lambda x: x.role_id, roles_to_preserve))
        roles_to_add = roles_ids - roles_ids_to_preserve

        for role_id in roles_to_add:
            role = self._role_repository.find_by_id_or_create(role_id)
            new_user_role = UserRole(user_id=user.id, role_id=role.id)
            self.save(new_user_role)

    def save(self, entity: UserRole) -> None:
        self._session.add(entity)
