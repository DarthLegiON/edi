from typing import Optional

from sqlalchemy import ForeignKey, Integer, String, BigInteger
from sqlalchemy.orm import relationship, mapped_column, Mapped

from db import Base, BaseRepository


class RoleGroup(Base):
    __tablename__ = 'user_roles_groups'

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    give_role_channel_id: Mapped[int] = mapped_column(BigInteger)
    give_role_message_id: Mapped[int] = mapped_column(BigInteger)


class RoleGroupRepository(BaseRepository):
    def get_group_by_message_id(self, message_id: str) -> Optional[RoleGroup]:
        return self._session.query(RoleGroup).filter(RoleGroup.give_role_message_id == str(message_id)).first()


class RoleReaction(Base):
    __tablename__ = 'user_roles_reactions'

    id: Mapped[int] = mapped_column(Integer, primary_key=True) #pylint: disable=unsubscriptable-object
    group: Mapped[RoleGroup] = relationship(RoleGroup) #pylint: disable=unsubscriptable-object
    role_name: Mapped[str] = mapped_column(String) #pylint: disable=unsubscriptable-object
    reaction_name: Mapped[str] = mapped_column(String) #pylint: disable=unsubscriptable-object
    group_id: Mapped[int] = mapped_column(ForeignKey('user_roles_groups.id')) #pylint: disable=unsubscriptable-object

class RoleReactionRepository(BaseRepository):
    def find_role_reaction_by_reaction_name(self, reaction_name: str) -> Optional[RoleReaction]:
        query = self._session.query(RoleReaction).filter(RoleReaction.reaction_name == reaction_name)
        return query.first()

    def find_role_reaction_by_role_name(self, role_name: str) -> Optional[RoleReaction]:
        return self._session.query(RoleReaction).filter(RoleReaction.role_name == role_name).first()
