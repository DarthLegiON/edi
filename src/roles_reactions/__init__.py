from typing import Optional

import discord

from helpers.emoji import EmojiHelper
from roles_reactions.db import RoleGroup, RoleReactionRepository


class ReactionRoleFinder:
    def __init__(self, role_reaction_repository: RoleReactionRepository) -> None:
        self.role_reaction_repository = role_reaction_repository

    def find_role_by_reaction_emoji(self, reaction_emoji: str) -> Optional[str]:
        role_reaction = self.role_reaction_repository.find_role_reaction_by_reaction_name(reaction_emoji)
        if role_reaction is not None:
            return role_reaction.role_name
        return None

    def find_reaction_emoji_by_role(self, role_name: str, guild: discord.Guild) -> Optional[discord.Emoji]:
        role_reaction = self.role_reaction_repository.find_role_reaction_by_role_name(role_name)
        if role_reaction is not None:
            return EmojiHelper.find_emoji_by_name(role_reaction.reaction_name, guild)
        return None

    def find_reaction_group(self, reaction_emoji: str) -> Optional[RoleGroup]:
        role_reaction = self.role_reaction_repository.find_role_reaction_by_reaction_name(reaction_emoji)
        if role_reaction is not None:
            return role_reaction.group
        return None

    def is_reaction_in_group(self, reaction_emoji: str, group_id: int) -> bool:
        role_reaction = self.role_reaction_repository.find_role_reaction_by_reaction_name(reaction_emoji)
        if role_reaction is not None:
            return role_reaction.group_id == group_id
        return False
