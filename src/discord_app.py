import logging
import sys

import discord

from bot.edibot import EdiBot
from config import settings, stream_log_handler, sentry_init
from di import Application

sentry_init()

logging.root.addHandler(stream_log_handler)

intents = discord.Intents.all()  # pylint: disable=assigning-non-slot
intents.members = True
intents.reactions = True

if __name__ == "__main__":
    container = Application()
    container.init_resources()  # pylint: disable=no-member
    container.wire(modules=[sys.modules[__name__]])  # pylint: disable=no-member

try:
    bot = EdiBot(command_prefix="/", intents=intents)

    bot.run(settings['token'])
except Exception as exception:
    logging.exception(exception, exc_info=True)
    raise exception
