import json
import logging
from datetime import datetime
from sys import argv

import discord
from dependency_injector.wiring import inject
from discord import Client

from config import settings, stream_log_handler, sentry_init
from helpers.export import HistoryExporter

sentry_init()

logging.root.addHandler(stream_log_handler)

intents = discord.Intents.all()
intents.members = True
intents.messages = True
intents.message_content = True

client = Client(intents=intents)


@client.event
@inject
async def on_ready(
    history_exporter = HistoryExporter(),
):
    logging.info('Logged in as %s', client.user)

    try:
        channel_id = argv[1]
        after = argv[2]
        for guild in client.guilds:
            history = guild.get_channel(int(channel_id)).history(after=datetime.fromtimestamp(int(after)))
            exported = await history_exporter.export_history(history)
            with open(f'output/exported_data_{guild.id}.json', 'w+', encoding='utf-8') as f:
                json.dump(exported, f)

            f.close()

        logging.info('Data saved to %s', f.name)
    except Exception as ex:  # pylint: disable=broad-exception-caught
        logging.exception(ex, exc_info=True)
    finally:
        await client.close()


client.run(settings['token'])
