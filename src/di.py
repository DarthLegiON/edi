from dependency_injector import containers, providers

from bot.db import SettingRepository
from bot.di import ReactionContainer, UserContainer
from db import DatabaseManager
from dictionaries.di import DictionaryContainer


class Application(containers.DeclarativeContainer):
    config = providers.Configuration()

    database_manager = providers.Singleton(DatabaseManager)

    reactions: ReactionContainer = providers.Container(ReactionContainer)

    users: UserContainer = providers.Container(UserContainer)

    dictionaries: DictionaryContainer = providers.Container(DictionaryContainer)

    setting_repository: SettingRepository = providers.Factory(
        providers.Singleton(
            SettingRepository,
            database_manager=database_manager,
        )
    )
