from dependency_injector import containers, providers

from db import DatabaseManager
from dictionaries import DictionaryService
from dictionaries.db import PhraseRepository


class DictionaryContainer(containers.DeclarativeContainer):
    config = providers.Configuration()

    database_manager: DatabaseManager = providers.Singleton(DatabaseManager)

    phrase_repository: PhraseRepository = providers.Factory(
        PhraseRepository, database_manager=database_manager
    )

    dictionary_service: DictionaryService = providers.Factory(
        DictionaryService, phrase_repository=phrase_repository
    )
