from enum import Enum

from dictionaries.db import PhraseRepository


class Dictionary(Enum):
    GREETING = 'greeting'
    GREETING_REJOIN = 'greeting_rejoin'
    MEMBER_LEFT = 'member_left'

_DEFAULT_PHRASES = {
    Dictionary.GREETING: 'Привет, {0}!',
    Dictionary.GREETING_REJOIN: 'C возвращением, {0}!',
    Dictionary.MEMBER_LEFT: '{0} покинул сервер',
}

class EmptyDictionaryException(BaseException):
    dictionary_name: str

    def __init__(self, dictionary_name: str, *args: object) -> None:
        super().__init__(*args)
        self.dictionary_name = dictionary_name


class DictionaryService:
    phrase_repository: PhraseRepository

    def __init__(self, phrase_repository: PhraseRepository) -> None:
        self.phrase_repository = phrase_repository

    def get_random_phrase(self, dictionary: Dictionary) -> str:
        phrase = self.phrase_repository.get_random_by_dictionary_name(dictionary.value)

        if phrase is None:
            return _DEFAULT_PHRASES[dictionary]

        return phrase.text

    def is_in_dictionary(self, dictionary: Dictionary, phrase: str, case_insensitive: bool = False) -> bool:
        return self.phrase_repository.check_if_exists_in_dictionary(dictionary.value, phrase, case_insensitive)
