from typing import Optional

from sqlalchemy import ForeignKey, Integer, String, Text
from sqlalchemy.orm import Session, relationship, mapped_column, Mapped
from sqlalchemy.sql import func

from db import DatabaseManager, Base


class Dictionary(Base):
    __tablename__ = 'dictionaries'

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    name: Mapped[str] = mapped_column(String)
    phrases = relationship('Phrase', secondary='dictionaries_phrases', back_populates="dictionaries")


class DictionaryPhrase(Base):
    __tablename__ = 'dictionaries_phrases'

    dictionary_id: Mapped[int] = mapped_column(Integer, ForeignKey('dictionaries.id'), primary_key=True)
    phrase_id: Mapped[int] = mapped_column(Integer, ForeignKey('phrases.id'), primary_key=True)


class Phrase(Base):
    __tablename__ = 'phrases'

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    text: Mapped[str] = mapped_column(Text)
    dictionaries = relationship(Dictionary, secondary='dictionaries_phrases', back_populates="phrases")


class PhraseRepository:
    session: Session

    def __init__(self, database_manager: DatabaseManager) -> None:
        self.session = database_manager.get_session()

    def _get_query_by_dictionary_name(self, dictionary_name: str):
        return self.session.query(Phrase).join(DictionaryPhrase).join(Dictionary).filter(
            Dictionary.name == dictionary_name)

    def get_random_by_dictionary_name(self, dictionary_name: str) -> Optional[Phrase]:
        # pylint: disable=not-callable
        return self._get_query_by_dictionary_name(dictionary_name).order_by(func.random()).first()

    def check_if_exists_in_dictionary(self, dictionary_name: str, phrase: str, case_insensitive: bool = False) -> bool:
        if case_insensitive:
            return bool(self._get_query_by_dictionary_name(dictionary_name).filter(
                func.lower(Phrase.text) == phrase).exists())
        return bool(self._get_query_by_dictionary_name(dictionary_name).filter(Phrase.text == phrase).exists())
