import logging
import os
import sys

import sentry_sdk


def sentry_init():
    if os.getenv("SENTRY_DSN") != '':
        sentry_sdk.init(
            dsn=os.getenv("SENTRY_DSN"),
            environment=os.getenv("ENVIRONMENT"),
            release=os.getenv("VERSION"),
            enable_tracing=True,
            traces_sample_rate=1.0,
            profiles_sample_rate=1.0,
        )

settings = {
    'token': os.getenv('TOKEN'),
    'static_storage_base_url': os.getenv('STATIC_STORAGE_BASE_URL'),
    'static_storage_auth_token': os.getenv('STATIC_STORAGE_AUTH_TOKEN'),
    'database_url': f"postgresql://"
                    f"{os.getenv('POSTGRES_USER')}:"
                    f"{os.getenv('POSTGRES_PASSWORD')}@"
                    f"{os.getenv('POSTGRES_HOST')}/"
                    f"{os.getenv('POSTGRES_DB')}",
    'internal_api_auth_token': os.getenv('INTERNAL_API_AUTH_TOKEN'),
}

stream_log_handler = logging.StreamHandler(sys.stdout)
logging.root.setLevel(logging.INFO)
stream_log_handler.setLevel(logging.INFO)
stream_log_handler.setFormatter(logging.Formatter(
    '%(asctime)s [%(levelname)s] %(message)s'))
