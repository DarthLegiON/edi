from dependency_injector import containers, providers

from actions.channel_reporter import ChannelReporter
from actions.greeter import Greeter
from actions.members_left_reporter import MembersLeftReporter
from actions.roles_manager import RolesManager
from bot.db import SettingRepository
from bot.event_listeners import OnMessageListener, UserListener
from db import DatabaseManager
from dictionaries.di import DictionaryContainer
from domain.boosty import GetBoostyUsersByLevelsHandler, RoleBoostyLevelRepository
from domain.user import UserInfoUpdateHandler, UserLeftHandler, JoinUserHandler
from domain.user.db import UserRepository, UserRoleRepository, RoleRepository
from roles_reactions import ReactionRoleFinder
from roles_reactions.db import RoleGroupRepository, RoleReactionRepository


# pylint: disable=no-member
class ReactionContainer(containers.DeclarativeContainer):
    config = providers.Configuration()

    database_manager: DatabaseManager = providers.Singleton(DatabaseManager)

    role_reaction_repository: RoleReactionRepository = providers.Factory(
        RoleReactionRepository, database_manager=database_manager
    )

    role_group_repository: RoleGroupRepository = providers.Factory(
        RoleGroupRepository, database_manager=database_manager
    )

    roles_manager: RolesManager = providers.Factory(
        RolesManager)

    reaction_role_finder: ReactionRoleFinder = providers.Factory(
        ReactionRoleFinder, role_reaction_repository=role_reaction_repository
    )

    on_message_listener: OnMessageListener = providers.Factory(
        OnMessageListener,
        role_group_repository=role_group_repository,
        reaction_role_finder=reaction_role_finder,
        roles_manager=roles_manager,
    )


class UserContainer(containers.DeclarativeContainer):
    config = providers.Configuration()

    database_manager: DatabaseManager = providers.Singleton(DatabaseManager)

    setting_repository: SettingRepository = providers.Factory(
        SettingRepository, database_manager=database_manager
    )

    role_reaction_repository: RoleReactionRepository = providers.Factory(
        RoleReactionRepository, database_manager=database_manager
    )

    user_repository: UserRepository = providers.Factory(
        UserRepository, database_manager=database_manager
    )

    role_repository: RoleRepository = providers.Factory(
        RoleRepository, database_manager=database_manager
    )

    user_role_repository: UserRoleRepository = providers.Factory(
        UserRoleRepository, database_manager=database_manager, role_repository=role_repository
    )

    join_user_handler: JoinUserHandler = providers.Factory(
        JoinUserHandler, user_repository=user_repository, database_manager=database_manager
    )

    user_info_update_handler: UserInfoUpdateHandler = providers.Factory(
        UserInfoUpdateHandler, user_repository=user_repository, database_manager=database_manager,
        user_role_repository=user_role_repository
    )

    user_left_handler: UserLeftHandler = providers.Factory(
        UserLeftHandler, user_repository=user_repository, database_manager=database_manager
    )

    reaction_role_finder: ReactionRoleFinder = providers.Factory(
        ReactionRoleFinder, role_reaction_repository=role_reaction_repository
    )

    channel_reporter: ChannelReporter = providers.Factory(ChannelReporter)

    dictionary: DictionaryContainer = providers.Container(DictionaryContainer)

    greeter: Greeter = providers.Factory(
        Greeter,
        dictionary_service=dictionary.dictionary_service,
        setting_repository=setting_repository,
    )

    members_left_reporter: MembersLeftReporter = providers.Factory(
        MembersLeftReporter,
        channel_reporter=channel_reporter,
        dictionary_service=dictionary.dictionary_service,
        user_repository=user_repository,
    )

    on_user_listener: UserListener = providers.Factory(
        UserListener,
        setting_repository=setting_repository,
        reaction_role_finder=reaction_role_finder,
        members_left_reporter=members_left_reporter,
        greeter=greeter,
        user_info_update_handler=user_info_update_handler,
        join_user_handler=join_user_handler,
        user_left_handler=user_left_handler,
    )


class BoostyContainer(containers.DeclarativeContainer):
    users: UserContainer = providers.Container(UserContainer)

    role_boosty_level_repository: RoleBoostyLevelRepository = providers.Factory(
        RoleBoostyLevelRepository, database_manager=users.database_manager
    )

    get_boosty_users_by_levels_handler: GetBoostyUsersByLevelsHandler = providers.Factory(
        GetBoostyUsersByLevelsHandler, role_boosty_level_repository=role_boosty_level_repository
    )

class ExportContainer(containers.DeclarativeContainer):
    config = providers.Configuration()

    database_manager: DatabaseManager = providers.Singleton(DatabaseManager)
