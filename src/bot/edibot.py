#pylint: disable=broad-exception-caught
import logging

import discord
from dependency_injector.wiring import Provide
from discord.ext.commands import Bot

from bot.db import SettingRepository
from bot.event_listeners import OnMessageListener, UserListener
from db import DatabaseManager
from di import Application


# pylint: disable=arguments-differ
# pylint: disable=no-member
class EdiBot(Bot):
    def __init__(
            self,
            command_prefix,
            intents=None,
            setting_repository: SettingRepository = Provide[Application.setting_repository],
            database_manager: DatabaseManager = Provide[Application.database_manager],
    ):
        self.setting_repository = setting_repository
        self.database_manager = database_manager
        super().__init__(command_prefix=command_prefix, intents=intents)

    async def on_ready(self):
        logging.info('Logged in as %s', self.user)

    async def on_member_join(self, member: discord.Member,
                             listener: UserListener = Provide[Application.users.on_user_listener]):
        try:
            await listener.handle_user_joined(self, member)
        except Exception as e:
            self.database_manager.rollback()
            raise e

    async def on_member_remove(self, member: discord.Member,
                               listener: UserListener = Provide[Application.users.on_user_listener]):
        try:
            await listener.handle_user_removed(self, member)
        except Exception as e:
            self.database_manager.rollback()
            raise e

    async def on_raw_reaction_add(self, payload: discord.RawReactionActionEvent,
                                  listener: OnMessageListener = Provide[Application.reactions.on_message_listener]):
        try:
            await listener.handle_raw_reaction_add(self, payload)
        except Exception as e:
            self.database_manager.rollback()
            raise e

    async def on_raw_reaction_remove(self, payload: discord.RawReactionActionEvent,
                                     listener: OnMessageListener = Provide[Application.reactions.on_message_listener]):
        try:
            await listener.handle_raw_reaction_remove(self, payload)
        except Exception as e:
            self.database_manager.rollback()
            raise e

    async def on_member_update(self, before: discord.Member, after: discord.Member,
                               listener: UserListener = Provide[Application.users.on_user_listener]):
        try:
            await listener.handle_user_updated(self, before, after)
        except Exception as e:
            self.database_manager.rollback()
            raise e
