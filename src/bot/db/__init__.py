from typing import Dict, Optional

from sqlalchemy import Column, Text, String
from sqlalchemy.orm import Session

from bot.setting import Setting as SettingEnum
from db import DatabaseManager, Base


class Setting(Base):
    __tablename__ = 'settings'

    code = Column(String, primary_key=True)
    value = Column(Text, nullable=False)


class UnknownSettingException(Exception):
    pass


class SettingRepository:
    _session: Session
    _cache: Dict = {}

    def __init__(self, database_manager: DatabaseManager) -> None:
        self._session = database_manager.get_session()

    def get_value_or_fail_by_code(self, code: SettingEnum, ignore_cache: bool = False) -> str:
        if not ignore_cache and code.value in self._cache:
            return self._cache[code.value]
        setting: Optional[Setting] = self._session.query(Setting.value).filter(Setting.code == code.value).first()

        if setting is None:
            raise UnknownSettingException(f'Setting with code {code.value} not found')

        if not ignore_cache:
            self._cache[code.value] = setting.value
        return setting.value
