from enum import Enum


class Setting(Enum):
    GREETINGS_CHANNEL_ID = 'greetings_channel_id'
    SYSTEM_CHANNEL_ID = 'system_channel_id'
    MEMBERS_LEFT_CHANNEL_ID = 'members_left_channel_id'
    NEWS_CHANNEL_ID = 'news_channel_id'
