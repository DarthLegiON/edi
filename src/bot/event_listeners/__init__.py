import logging

from discord import Client, Guild, RawReactionActionEvent, Member, Role

from actions.exceptions import RoleNotFoundException
from actions.greeter import Greeter
from actions.members_left_reporter import MembersLeftReporter
from actions.roles_manager import RolesManager
from bot.db import SettingRepository
from bot.setting import Setting
from domain.user import UserInfoUpdateHandler, UserLeftHandler, JoinUserHandler
from helpers.reactions import ReactionsHelper
from roles_reactions import ReactionRoleFinder
from roles_reactions.db import RoleGroupRepository


class OnMessageListener:
    def __init__(
            self,
            role_group_repository: RoleGroupRepository,
            reaction_role_finder: ReactionRoleFinder,
            roles_manager: RolesManager,
    ) -> None:
        self.role_group_repository = role_group_repository
        self.reaction_role_finder = reaction_role_finder
        self.roles_manager = roles_manager

    async def handle_raw_reaction_add(self, client: Client, payload: RawReactionActionEvent) -> None:
        guild = client.get_guild(payload.guild_id)
        member = guild.get_member(payload.user_id)

        reaction_group = self.role_group_repository.get_group_by_message_id(
            str(payload.message_id))
        if reaction_group is not None:
            logging.info('Reaction adding captured: %s by %s', payload.emoji.name, member.display_name)
            role_name = self.reaction_role_finder.find_role_by_reaction_emoji(
                payload.emoji.name)
            if role_name is not None:
                if self.reaction_role_finder.is_reaction_in_group(payload.emoji.name, reaction_group.id):
                    try:
                        await self.roles_manager.assign_role_by_name(member, role_name)
                        return
                    except RoleNotFoundException as exception:
                        logging.info('Role not found by name "%s"', exception.role_name)
                else:
                    logging.info(
                        'Role not found in group %s', reaction_group.id)
            else:
                logging.info('Role not found by emoji %s', payload.emoji.name)
            await ReactionsHelper.remove_reaction_from_channel_message(
                client=client,
                member=guild.get_member(payload.user_id),
                channel_id=payload.channel_id,
                message_id=payload.message_id,
                emoji=payload.emoji
            )

    async def handle_raw_reaction_remove(self, client: Client, payload: RawReactionActionEvent):
        guild: Guild = client.get_guild(payload.guild_id)
        member: Member = guild.get_member(payload.user_id)

        reaction_group = self.role_group_repository.get_group_by_message_id(
            str(payload.message_id))
        if reaction_group is not None:
            logging.info('Reaction removal captured: %s by %s', payload.emoji.name, member.display_name)
            role_name = self.reaction_role_finder.find_role_by_reaction_emoji(
                payload.emoji.name)
            if role_name is not None and self.reaction_role_finder.is_reaction_in_group(payload.emoji.name,
                                                                                        reaction_group.id):
                try:
                    await self.roles_manager.revoke_role_by_name(member, role_name)
                except RoleNotFoundException as exception:
                    logging.info('Role not found by name "%s"', exception.role_name)
            else:
                logging.info('Role not found by emoji %s', payload.emoji.name)

 # pylint: disable=too-many-arguments
class UserListener:
    def __init__(
            self,
            setting_repository: SettingRepository,
            reaction_role_finder: ReactionRoleFinder,
            members_left_reporter: MembersLeftReporter,
            greeter: Greeter,
            user_info_update_handler: UserInfoUpdateHandler,
            join_user_handler: JoinUserHandler,
            user_left_handler: UserLeftHandler,
    ) -> None:
        self.setting_repository = setting_repository
        self.reaction_role_finder = reaction_role_finder
        self.members_left_reporter = members_left_reporter
        self.greeter = greeter
        self.user_info_update_handler = user_info_update_handler
        self.join_user_handler = join_user_handler
        self.user_left_handler = user_left_handler

    async def handle_user_updated(self, client: Client, before: Member, after: Member) -> None:
        self.user_info_update_handler.handle(after)
        deleted_roles: set[Role] = set(before.roles) - set(after.roles)
        for role in deleted_roles:
            logging.info('Role %s has been removed from user %s', role.name, after.display_name)
            emoji = self.reaction_role_finder.find_reaction_emoji_by_role(
                role.name, after.guild)
            if emoji is not None:
                reaction_group = self.reaction_role_finder.find_reaction_group(
                    emoji.name)
                await ReactionsHelper.remove_reaction_from_channel_message(
                    client=client,
                    member=after,
                    channel_id=reaction_group.give_role_channel_id,
                    message_id=reaction_group.give_role_message_id,
                    emoji=emoji
                )
            else:
                logging.debug('Reaction not found for role %s', role.name)

    async def handle_user_joined(self, client: Client, member: Member) -> None:
        if member.id != client.user.id:
            logging.info('Member joined: %s', member.name)
            join_handler_result = await self.join_user_handler.handle(member)

            greetings_channel_id = int(self.setting_repository.get_value_or_fail_by_code(Setting.GREETINGS_CHANNEL_ID))
            channel = client.get_channel(greetings_channel_id)
            if channel is None:
                logging.error('Cannot find channel: %s', greetings_channel_id)
                return
            if channel.guild.id == member.guild.id:
                await self.greeter.greet(member, channel, join_handler_result.is_new)

    async def handle_user_removed(self, client: Client, member: Member) -> None:
        self.user_left_handler.handle(member.id)
        logging.info('Member left: %s', member.name)
        members_left_channel_id = int(
            self.setting_repository.get_value_or_fail_by_code(Setting.MEMBERS_LEFT_CHANNEL_ID))
        channel = client.get_channel(members_left_channel_id)
        if channel is None:
            logging.error('Cannot find channel: %s', members_left_channel_id)
            return
        await self.members_left_reporter.report(member, channel)
