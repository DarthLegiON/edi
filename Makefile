all: start

rebuild:
	docker-compose down && docker-compose build

start-app:
	docker-compose up app

start-api:
	docker-compose up api

start-db:
	docker-compose up db -d

lint:
	docker-compose run --rm -e PYTHONPATH=./src app pylint --recursive=y --rcfile .pylintrc ./src

shell:
	docker-compose run --rm app bash

migrate:
	docker-compose run --rm app alembic upgrade head