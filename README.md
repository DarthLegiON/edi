# EDI
[![pipeline status](https://gitlab.com/DarthLegiON/edi/badges/master/pipeline.svg)](https://gitlab.com/DarthLegiON/edi/-/commits/master)

## Installation

1. Install Docker
2. Install Docker Compose
3. Create network:
```
docker network create edi
```

4. Acquire the bot token [here](https://discord.com/developers/).
5. Create a copy of `.env.example` file named `.env`. Put the acquired token there in the TOKEN line.
6. Set your own passwords, usernames and database name for PostgreSQL.
7. Run
```
docker-compose up
```
for test to see the application log. Or start the bot as a deamon:
```
docker-compose up -d
```
