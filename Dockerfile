FROM python:3.10.5 as base

WORKDIR /app

COPY ./requirements.txt ./requirements.txt

RUN pip install --upgrade pip && \
    pip install --no-cache-dir --progress off -r requirements.txt

COPY ./alembic.ini ./alembic.ini
COPY ./src ./src
RUN mkdir output

CMD ["python", "-u", "src/discord_app.py"]

FROM base as dev

COPY ./requirements-dev.txt ./requirements-dev.txt
COPY ./.pylintrc ./.pylintrc

RUN pip install --progress off -r requirements-dev.txt
