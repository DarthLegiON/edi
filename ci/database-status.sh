alembic-autogen-check > diff.txt \
&&
if grep -q "No changes detected in your mapping information." diff.txt;
then
  echo 'Migrations in sync.'
  exit 0;
fi
if grep -q "Target database is not up to date." diff.txt;
then
  echo 'You have available migrations to execute'
  exit 1;
fi
GENERATED_LINE=$(grep "Generated new migration class" diff.txt)
if [ "$GENERATED_LINE" != "" ];
then
  echo $GENERATED_LINE
  cat ${GENERATED_LINE:35:-1}
  exit 1;
fi
